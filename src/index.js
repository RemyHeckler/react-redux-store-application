/* eslint-disable */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import 'basscss/css/basscss.min.css';
import reducers from './reducers';
import ShoppingList from './pages/ShoppingList';
import BasketList from './pages/BasketList';

import registerServiceWorker from './registerServiceWorker';


const store = compose(applyMiddleware(createLogger()))(createStore)(reducers);

const Content = () => (
  <Switch>
    <Route exact path="/" component={ShoppingList} />
    <Route path="/basket" component={BasketList} />
  </Switch>
);

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <div>
        <Content />
      </div>
    </BrowserRouter>

  </Provider>,
  document.getElementById('root'),
);

registerServiceWorker();
