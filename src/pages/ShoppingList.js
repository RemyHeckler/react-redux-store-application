import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import GoodsList from '../containers/goodsList';
import Image from '../containers/image';
import Menu from '../containers/menu';
import HeadTable from '../components/headTable';

import '../style/ShoppingList.css';

class ShoppingList extends Component {
  state = {
    showMenu: false,
  }

  handleShowMenuClick = () => {
    this.setState({ showMenu: !this.state.showMenu });
  }

  render() {
    return (
      <div>
        <header className="flex justify-start align-baseline">
          <div className="col-2 flex justify-center items-center">
            <button className="navigation" onClick={this.handleShowMenuClick}>
              <h3 className="center">
                <i className="ion-navicon-round" />
              Navigation
              </h3>
            </button>
          </div>
          <div className="col-10">
            <h2 className="center">Shopping List</h2>
          </div>
        </header>
        <div className="flex justify-start">
          <ReactCSSTransitionGroup
            transitionName="left-aside"
            transitionAppear={false}
            transitionEnterTimeout={500}
            transitionLeaveTimeout={300}
          >
            {this.state.showMenu &&
            <aside
              className="col-12 flex-column left-side relative"
            >
              <Menu />
            </aside>}
          </ReactCSSTransitionGroup>
          <div className="col-12">
            <HeadTable />
            <GoodsList />
          </div>
        </div>
        <div className="col-3 fixed top-0 left-0 mt2 ml4">
          <Image />
        </div>
      </div>
    );
  }
}

export default ShoppingList;
