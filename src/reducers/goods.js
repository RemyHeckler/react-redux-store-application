const initialState = [
  {
    id: 1,
    title: 'milk',
    pricePerOne: 0.99,
    img: './images/milk.jpg',
  }, {
    id: 2,
    title: 'apples',
    pricePerOne: 0.45,
    img: './images/apples.jpg',
  }, {
    id: 3,
    title: 'eggs',
    pricePerOne: 0.63,
    img: './images/eggs.jpg',
  }, {
    id: 4,
    title: 'bread',
    pricePerOne: 0.42,
    img: './images/bread.jpg',
  }, {
    id: 5,
    title: 'cheese',
    pricePerOne: 0.79,
    img: './images/cheese.jpg',
  }, {
    id: 6,
    title: 'meat',
    pricePerOne: 1.22,
    img: './images/meat.jpg',
  }, {
    id: 7,
    title: 'sugar',
    pricePerOne: 0.27,
    img: './images/sugar.jpg',
  }, {
    id: 8,
    title: 'tomato',
    pricePerOne: 0.54,
    img: './images/tomato.jpg',
  }];

const tasks = (state = initialState) => state;

export default tasks;
