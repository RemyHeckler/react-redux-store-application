import { combineReducers } from 'redux';

import goods from './goods';
import selectedGood from './selectedGood';
import basketGoods from './basketGoods';
import search from './search';

const reducers = combineReducers({
  goods,
  selectedGood,
  basketGoods,
  search,
});

export default reducers;
