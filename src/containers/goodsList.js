import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import { select } from '../actions/select';
import { add } from '../actions/add';
import { search } from '../actions/search';

import Product from '../components/product';
import SearchBar from '../components/searchBar';


class GoodsList extends Component {
  static propTypes = {
    goods: PropTypes.array,
    select: PropTypes.func,
    search: PropTypes.func,
    add: PropTypes.func,
  }

  state = {
    serachValue: '',
  };

  handleChangeSearch = (event) => {
    this.setState({
      serachValue: event.target.value,
    });
  }

  handleSearchClick = (event) => {
    event.preventDefault();
    this.props.search(this.state.serachValue);
  }

  handleAddItem = (item, event) => {
    event.preventDefault();
    this.props.add(item);
  }

  render() {
    return (
      <div className="center">
        <SearchBar
          onSearchClick={this.handleSearchClick}
          onChangeSearch={this.handleChangeSearch}
        />
        {this.props.goods.map(item =>
          (<Product
            key={item.id}
            data={item}
            onShowImageClick={() => this.props.select(item)}
            onAddItem={(good, event) => this.handleAddItem(good, event)}
          />))}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    goods: state.goods
      .filter(item => !state.basketGoods.map(good => good.id).includes(item.id))
      .filter(item => item.title.includes(state.search)),
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    select,
    add,
    search,
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(GoodsList);
