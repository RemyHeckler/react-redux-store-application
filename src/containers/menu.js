import React from 'react';
import { Link } from 'react-router-dom';

function Menu() {
  return (
    <div className="menu">
      <Link to="/">
        <div className="p2">
          Shopping List
        </div>
      </Link>
      <Link to="/basket">
        <div className="p2">
          Basket
        </div>
      </Link>
    </div>
  );
}
export default Menu;
