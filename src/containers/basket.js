import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import { save } from '../actions/save';
import { dlt } from '../actions/delete';

import ProductInBasket from '../components/productInBasket';

const Basket = data => (
  <div className="center">
    {data.basketGoods.map(item =>
      (<ProductInBasket
        key={item.id}
        data={item}
        onDeleteClick={id => data.dlt(id)}
        onSaveClick={good => data.save(good)}
      />))}
    {data.basketGoods.length ?
      <div className="flex justify-between items-baseline total">
        <div className="col-6">
          <h4>Total</h4>
        </div>
        <div className="col-3">
          {data.basketGoods.length ?
            Math.ceil(data.basketGoods
              .map(item => item.pricePerOne * item.ammount)
              .reduce((a, b) => a + b) * 100) / 100
            :
            null
          }
        </div>
        <div className="col-3" />
      </div>
      :
      null
    }
  </div>
);

Basket.propTypes = {
  data: PropTypes.shape({
    basketGoods: PropTypes.array,
    dlt: PropTypes.func,
    save: PropTypes.func,
  }),
};

function mapStateToProps(state) {
  return {
    basketGoods: state.basketGoods,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    dlt,
    save,
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Basket);
