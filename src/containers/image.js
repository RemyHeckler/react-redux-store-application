import React from 'react';
import { connect } from 'react-redux';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';

import { select } from '../actions/select';

const Image = data => (
  <ReactCSSTransitionGroup
    transitionName="example"
    transitionAppear
    transitionAppearTimeout={500}
    transitionEnterTimeout={300}
    transitionLeaveTimeout={300}
  >
    {data.selectedGood &&
      <div className="flex items-start">
        <img
          src={data.selectedGood.img}
          alt={data.selectedGood.title}
          width="100%"
        />
        <button onClick={() => data.select(data.selectedGood)} className="close-button">
          <i className="ion-close" />
        </button>
      </div>
    }
  </ReactCSSTransitionGroup>
);

Image.propTypes = {
  selectedGood: PropTypes.shape({
    img: PropTypes.string,
    title: PropTypes.string,
  }),
};

function mapStateToProps(state) {
  return {
    selectedGood: state.selectedGood,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    select,
  }, dispatch);
}


export default connect(mapStateToProps, matchDispatchToProps)(Image);
