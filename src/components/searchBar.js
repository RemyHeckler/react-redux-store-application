import React from 'react';
import PropTypes from 'prop-types';

const SearchBar = ({ onSearchClick, onChangeSearch }) => (
  <form className="flex justify-center search-bar" onSubmit={onSearchClick}>
    <div className="col-1 mr2">Search:</div>
    <input placeholder="Input search value" className="col-2 mr2" onChange={onChangeSearch} />
    <button type="submit" className="col-1 mr2">Search</button>
  </form>
);

SearchBar.propTypes = {
  onChangeSearch: PropTypes.func,
  onSearchClick: PropTypes.func,
};

export default SearchBar;
