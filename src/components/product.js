import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Product extends Component {
  static propTypes = {
    data: PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      pricePerOne: PropTypes.number,
    }),
    onShowImageClick: PropTypes.func,
    onAddItem: PropTypes.func,
  }

  state = {
    id: this.props.data.id,
    title: this.props.data.title,
    ammount: 1,
    pricePerOne: this.props.data.pricePerOne,
  };

  handleChangeAmmount = (event) => {
    this.setState({ ammount: +event.target.value });
  }

  render() {
    const { onShowImageClick, onAddItem } = this.props;
    return (
      <form onSubmit={event => onAddItem(this.state, event)} className="flex justify-between good" key={this.state.id}>
        <div className="col-3">{this.state.title}</div>
        <div className="col-3">
          <input
            className="col-4"
            type="number"
            min="1"
            defaultValue={this.state.ammount}
            onChange={this.handleChangeAmmount}
          />
        </div>
        <div className="col-3">
          {Math.ceil(this.state.pricePerOne * this.state.ammount * 100) / 100}
        </div>
        <div className="col-3 flex justify-center">
          <button className="col-4" type="button" onClick={onShowImageClick}>Show image</button>
          <button className="col-4" type="submit">Add</button>
        </div>
      </form>
    );
  }
}

export default Product;
