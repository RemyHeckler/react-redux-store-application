import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ProductInBasket extends Component {
  static propTypes = {
    data: PropTypes.shape({
      id: PropTypes.number,
      ammount: PropTypes.number,
      title: PropTypes.string,
      pricePerOne: PropTypes.number,
    }),
    onSaveClick: PropTypes.func,
    onDeleteClick: PropTypes.func,
  }

  state = {
    id: this.props.data.id,
    title: this.props.data.title,
    ammount: this.props.data.ammount,
    pricePerOne: this.props.data.pricePerOne,
    edit: false,
  }

  handleChangeAmmount = (event) => {
    this.setState({ ammount: +event.target.value });
  }

  handleSelectEdit = () => {
    this.setState({
      id: this.props.data.id,
      title: this.props.data.title,
      ammount: this.props.data.ammount,
      pricePerOne: this.props.data.pricePerOne,
      edit: !this.state.edit,
    });
  }

  handleSaveClick = (event) => {
    event.preventDefault();
    const product = {
      id: this.state.id,
      title: this.state.title,
      ammount: this.state.ammount,
      pricePerOne: this.state.pricePerOne,
    };
    this.props.onSaveClick(product);
    this.setState({ edit: false });
  }

  render() {
    const { onDeleteClick } = this.props;
    return (
      this.state.edit ?
        <form onSubmit={this.handleSaveClick} className="flex justify-between edit">
          <div className="col-3">{this.state.title}</div>
          <div className="col-3">
            <input
              type="number"
              className="col-4"
              min="1"
              defaultValue={this.state.ammount}
              onChange={this.handleChangeAmmount}
            />
          </div>
          <div className="col-3">
            {Math.ceil(this.state.pricePerOne * this.state.ammount * 100) / 100}
          </div>
          <div className="col-3 flex justify-center">
            <button className="col-4" type="submit">Save</button>
            <button className="col-4" type="button"onClick={this.handleSelectEdit}>Cancel</button>
          </div>
        </form>
        :
        <div className="flex justify-between good">
          <div className="col-3">{this.state.title}</div>
          <div className="col-3">{this.state.ammount}</div>
          <div className="col-3">
            {Math.ceil(this.state.pricePerOne * this.state.ammount * 100) / 100}
          </div>
          <div className="col-3 flex justify-center">
            <button className="col-4" onClick={this.handleSelectEdit}>Edit</button>
            <button className="col-4" onClick={() => onDeleteClick(this.state.id)}>Delete</button>
          </div>
        </div>
    );
  }
}

export default ProductInBasket;
